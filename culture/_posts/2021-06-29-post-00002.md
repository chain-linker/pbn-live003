---
layout: post
title: "다크홀 인물관계도 원작 웹툰 변종인간 관전포인트 (ocn 금토드라마)"
toc: true
---

## 

## 다크홀 인물관계도 원작 웹툰
 변종인간 관전포인트
(ocn 금토드라마)
싱크홀에서 나온 의문의
검은 연기를 마신 변종인간들,
그들 사이에서
살아남은 자들의 처절한 생존기
크리처 액션 스릴러
크리처: 특정한 존재나 괴물을 뜻하는
 'Creature'과 작품+물(物)의 합성어.
 

 

 +2021년 4월 30일 첫방 12부작+
(ocn 금토드라마)
ocn, tvN 밤 10시 50분~
기획 OCN
 제작사 ㈜영화사우상, ㈜키위미디어그룹
연출 김봉주
각본 정이도
 

 김옥빈, 이준혁 외
 

 

### 

#### ocn 유니버스
 

 금차 ocn 드라마 다크홀은 드라마틱 시네마 영륜 5번째 작품이다. 간단하게 줄여서 ocn 유니버스라고 부른다.
마치 마블 유니버스처럼 다양한 영화의 캐릭터들이 서로의 세계관을 공유하며 스토리를 진행하는 방식을 표방하고 있지만 ocn 유니버스에서는 아직까지 뚜렷한 주견 공유는 없었다.
 

 

 그러다 금번 다크홀에서 슬며시 변화가 있었는데 희곡 구해줘에서 가짜 신도 임주호 역을 맡았던 정해균, 뿐만 아니라 타인은 지옥이다에서 소정화 순경 역을 맡았던 안은진이 같은 캐릭터로 다크홀에 출연하게 되었다.
 

 ocn 유니버스 작품
1. 트랩
2. 타인은 지옥이다.
3. 번외수사
4. 써치
5. 다크홀
 

 

### 

#### ocn 다크홀 줄거리

 다크홀은 무지광역시라는 가상의 도시에서 벌어지는 이야기다. 대규모 석유화학공장 단지가 있는 무지광역시 숲 속에 대형의 싱크홀이 생기는데, 네놈 싱크홀에서는 검은 연기가 거듭 올라온다.
그런데 과실 검은 연기는 사람들의 몸속으로 들어가서 그편 사람들의 내면 깊숙한 공포를 끄집어내어 환상을 보게 만든다. 그쯤 인간성을 잃고 난폭해지는 사람들을 통칭 변종인간이라고 부른다.
사람들을 변종인간으로 만드는 검은 연기에 고립된 무진광역시, 인제 속에서 진실을 찾고 생존을 위해 싸우는 인물들에 대한 이야기다.
 

 

### 

#### ocn 다크홀 변종인간 증상

 다크홀에서 나온 검은 연기를 마시고 변해버린 변종인간은 증상에 따라 크게 3단계로 나눌 목숨 있다.
 

 1단계
연기를 들이마시는 일찰나 두 눈에 검은 연기가 서리게 되고, 양반 공포스러웠던 기억이 주의 앞에 환상으로 나타나서 극도의 공포감을 느끼는 단계.
2단계
얼굴의 반쪽이 검게 변한채 환각에 빠진다. 목용 앞에 타인을 자신의 트라우마 고갱이 공포의 대상으로 인지하고 무차별적인 공격 성향을 보이게 된다.
3단계
최종 성장 단계. 단특 귀에서 작은 촉수 같은 것이 나와서 조종을 당하는 상태. 검은 연기로 사람들을 변하게 만드는 수하 특정 존재의 직접적인 명령을 받아서 움직이는데, 살아남은 사람들 속에 숨어 공포와 혼란을 조장하게 된다.

 

### 

#### ocn 다크홀 원작 웹툰

 근시 NC버프툰에서 다크홀과 세계관을 함께하는 웹툰이 연재를 시작했다. 보통 이런 장르물은 원작이 개개 있고 노형 원작이 웹툰인 경우가 많았지만 ocn 다크홀은 정이도 작가의 순수 창작물이다.
정이도 작가는 왕석 싱크홀을 소재로 새로운 작품을 결의 중이라고 밝혔었는데 그게 다크홀이다. NC버프툰에서 연재 군중 다크홀은 드라마 다크홀의 스핀오프 같은 내용으로 희곡 송신 기념 웹툰인 셈이다.
 

 

### 

#### 다크홀 인물관계도

 

 다크홀 등장인물

### 

#### 주요인물

 이화선 게다가 (김옥빈)
연기를 마시고 변하지 않은, 유일한 생존자.
서울지방청 광수대 형사.
수사하고 있던 사건의 용의자에게 유일하게 가족이 되어준 남편마저 살해당하자 사랑하는 사람을 지키지 못했다는 트라우마에 빠지게 된다.
그러던 어느 때 남편을 죽인 살인마 이수연의 전화를 받고 내려가게 된 무지시에서 검은 연기를 마시고 변종인간이 되어버린 사람들을 목격하게 되고, 손수 짐짓 문제의 검은 연기를 마시게 된다.
하지만 화선은 변종이 된 사람들과는 달리 연기를 마시고도 변하지 않고 공포와 맞서 싸워나가며 다른 생존자들과 아울러 잃어버린 일상을 되찾는 데 앞장선다.
 

 동료들도 인정한 자타공인 굳건한 정식력의 소유자.
 

 

 유태한 더구나 (이준혁)
 죽음을 두려워하지 않는, 강인한 생존자.
現 렉카 기사, 前 경찰.
 

 화선의 부 큰 조력자임과 동시에 검은 연기를 들이마신 화선이 다른 변종인간들처럼 변하지 않도록 끝까지 이놈 옆을 지켜주는 정서적 버팀목이 되는 인물.

천하태평한 성격에 농담과 장난을 즐기는 껄렁이처럼 보이지만, 실상 누구보다 강인한 정의감을 갖고 있다.
오해가 얽힌 불미스러운 일로 경순 제복을 벗기는 했지만, 상천 쎄리 시절에 대한 자부심을 갖고 있는 인물이다.
그래서 변종들로 인해 무지시가 아비규환에 빠지자, 우연히 백난 형사 화선과 아울러 누구보다 적극적으로 사람들을 구하기 위해 애쓴다.
 

 

### 

#### 당신 노천 인물

 박순일 직분 (임원희)
 인간적 면모가 다분한, 현실적 조력자.
 무지시 지구대 경장.
혼란에 빠진 무지시에서 사람들을 구하기 위해 고군분투하는 태한에게 화선 다음으로 주인 큰 도움을 주는 조력자이다. 텅 사정에 밝고 촌 사건에 솔선수범하는 경찰이자 무지고등학교 최경수 이사장의 뒤처리 미상불 마다하지 않는 경찰.
 

 

 이수연 역 (?? 미상)
 연쇄살인마
화선의 남편을 살해 사과후 화선에게 조롱하듯 전화를 걸어 자신이 있는 무지시로 내려오게 만든 인물이다. 하얀 얼굴, 긴 생머리에 보호본능을 자극하는 여린 모습이지만, 사실은 약물로 사람을 죽인 대미 얼굴에 흰 복면을 씌워 붉은 립스틱으로 장난스럽게 스마일 표시를 그려 넣는 사이코패스다.
 

 조현호 역할 (조지안)
지구대 순경
사회 초년생답게 싹싹하고 버르장머리 바른 모습의 경찰, 혹 애 같은 엉뚱함과 허당끼의 소유자. 곧바로 태어날 아이를 기다리며 행복한 생활을 하고 있는데 무지시에 검은 연기가 들이닥치면서 아내가 있는 집으로 돌아가지 못하고 박순일 경장과 다름없이 하 병원에 갇히게 된다.
 

 김선녀 배치 (송상은)
무지시 무당
엄마를 대신해 신내림까지 받아가며 일생을 헌신했건만 예고도 궁핍히 신빨이 사라지자 내림굿을 받았던 선녀신을 증오하게 된다. 그만치 절망에 빠져있던 이이 앞에 정체를 알 복 없는 연기와 다름없이 새로운 신이 나타나게 되며, 그는 더욱더욱 큰 힘을 얻기 위해 새로운 신을 받아들이기로 한다.
 

 

 최경수 불찬 (김병기)
 무지고교 이사장
피도 눈물도 없는 국가기관 고문기술자에서 융통 좋은 양지 유지. 교육재단 이사장까지 하이패스로 달려온 인물. 자신의 권력과 재물 앞에 머리를 조아리는 사람들을 보며 이를 호감 즐기며 살아왔다.
 

 한동림 역 (오유진)
 무지고교 학생
부모님이 사고로 돌아가신 나중 독이 할머니를 부양해야 하는 사례 심원 마을에서도, 학교에서도, 첫머리 힘없는 자신을 이용하려는 사람들뿐이지만 무너지지 않기 위해 사력을 다하며 하루하루를 버텨내는 중.
 

 윤샛별 또한 (이하은)
무지병원 간호사
얼마 전 서울에서 무지병원으로 로테이션 온 간호사. 연약해 보이는 겉모습과 달리 간호사로서의 사명감을 다해 도망치지 않고 병원에 지아비 환자들을 돕는다. 태한의 친구, 영식과 짧은 인연이 있다.
 

 

 최승태 역시 (박근록)
무지고교 교사
모범적이고 겸손한 인성 덕에 동무 선생님들과 학생들, 학부모들 사이에서 명망이 두텁다. 패닉의 상황에서 사람들을 구하기 위해 과도하리만큼 애쓰는 인물.
 

 이진석 항변 (김도훈)
무지고교 퇴학생
퇴학을 당한 결실 지금까지 마을 양아치로 살아왔다. 동림을 비롯한 불우한 가정의 여학생들이나 가출한 여학생들을 이용해 쉽게 돈을 벌어 보려고 한다. 동림을 끈질기게 쫓아다니며 괴롭히는 인물.
 

 정도윤 더욱이 (이예빛)
개하초 1학년
검은 연기를 마시고 변해버린 사람들 속에서 엄마를 잃고 화선에 의해 극적으로 살아남게 된 아이. 처음에는 자신을 불편해하던 화선과 생사고락을 같이 하게 되면서 차츰 서로에게 쥔님 의지가 되는 관계로 발전해 나간다.
 

 

### 

#### 다크홀 관전포인트
 1. 연쇄살인마 이수연

 다크홀은 싱크홀을 소재로 한 장르물이다. 싱크홀에서 올라온 검은 연기로 사람들이 변종인간화된다는 소재만으로도 흥미롭지만 드라마에는 연쇄살인마 이수연이라는 캐릭터가 등장한다.
이수연은 사람을 죽이고 임자 머리에 스마일 표시가 그려진 가면을 씌우는 인물이다. 남달리 이수연은 극진지두 중 이화선으로 등장하는 김옥빈의 남편을 죽이기도 했다.
 

 광수대 형사인 이화선은 남편을 죽인 범인이자 연쇄살인마 이수연을 기능 위해 혈안이 되어 있는데, 이수연은 이화선에게 '나 보고 싶으면 이쪽으로 와요'라는 문자를 보내서 무지광역시로 오게 만들었다. 무지광역시에서 벌어지는 검은 연기와 연쇄살인마 이수연은 아무개 관련이 있는 걸까? 호위호 하필 이화선을 무지시로 불러들였을까.
 

 관전포인트 2. 면역자 이화선

 김옥빈이 맡은 이화선은 광수대 형사이자 남편을 죽인 범인을 기예 위해 무지시로 오게 되는 인물이다. 더구나 사람들을 변종인간으로 만드는 검은 연기에 대해 알게 되고 사건을 해결해 나가게 된다.
그런데 이화선은 검은 연기에 면역을 가지고 있다. 보통의 사람들은 검은 연기 왜냐하면 변하게 되는데 유일하게 이화선만 변종이 되지 않은 것.
검은 연기를 통해 사람들을 조종하는 자, 또한 그런 [웹툰 추천](https://representativeconfused.ml/culture/post-00000.html)검은 연기를 마셔도 변종이 되지 않는 면역자 이화선, 이전 두 사람이 희곡 다크홀의 미스터리를 풀어나가는 두 축이 될 것 같다.
 

 

 사진출처: ocn 다크홀
 ⓒ곰곰지영의 글, 사진 [무단도용, 복제, 재배포 금지]
 

#### '엔터+ > 드라마' 카테고리의 다른 글
